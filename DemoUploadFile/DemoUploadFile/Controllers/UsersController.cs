﻿using System.Collections.Generic;
using System.IO;
using DemoUploadFile.Models;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoUploadFile.Controllers
{
    public class UsersController : Controller
    {
        [HttpGet]
        public IActionResult Index(List<User> users = null)
        {
            users = users == null ? new List<User>() : users;
            return View(users);
        }
        [HttpPost]
        [System.Obsolete]
        public IActionResult Index(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        {
            string fileName = $"{ hostingEnvironment.WebRootPath}\\files\\{file.FileName}";
            using (FileStream fileStream = System.IO.File.Create(fileName))
            {
                file.CopyTo(fileStream);
                fileStream.Flush();

            }
            var users = this.GetStudentList(file.FileName);
            return Index(users);
        }
        private List<User> GetStudentList(string fName)
        {
            List<User> users = new List<User>();
            var fileName = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\files"}" + "\\" + fName;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    while (reader.Read())
                    {
                        users.Add(new User()
                        {
                            FullName = reader.GetValue(0).ToString(),
                            PhoneNumber = reader.GetValue(1).ToString(),
                            Email = reader.GetValue(2).ToString()
                        });
                    }
                }
            }
            return users;
        }
    }
}
